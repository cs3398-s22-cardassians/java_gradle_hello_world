package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

//adding a comment
// Starting class (3-4)


public class TestGreeter {

   private Greeter g = new Greeter();

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Jayla is Graduating")
   public void testGreeterJayla() 

   {
      g.setName("Jayla is Graduating");
      assertEquals(g.getName(),"Jayla is Graduating");
   }

   @Test
   @DisplayName("Test for Reve")
   public void testGreeterReve()
   {
      g.setName("Reve");
      boolean yNull = false;
      if (g.getName() == null) {
      yNull = true;
      }
      assertFalse(yNull, "This will pass");
   }

   @Test
   @DisplayName("Test for Lauren")
   public void testGreeterLauren() 

   {
      g.setName("Lauren");
      assertEquals(g.getName(),"Lauren");
      assertEquals(g.sayHello(),"Hello Lauren!");
   }
   

   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {
      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

   @Test
   @DisplayName("Test for Name = 'Steve'")
   public void testGreeterSteve()
   {
       g.setName("Steve");
       assertEquals(g.getName(), "Steve");
       assertEquals(g.sayHello(), "Hello Steve!");
   }

   @Test
   @DisplayName("Test for Name = 'Steve Tchagou'")
   public void testGreeterST() {
       g.setName("Steve Tchagou");
       String name = g.getName().trim();
       boolean upper = false;

       if(name == "SteveTchagou") {
           upper = true;
       }

       assertFalse(upper, "This will fail");
   }

   @Test
   @DisplayName("Test for Name = 'Assignment14'")
   public void testGreeterLeo()
   {
      g.setName("Assignment14");
      assertEquals(g.getName(),"Assignment14");
     // assertEquals(g.sayHello(),"Hello Assignment14");
   }

    @Test
    @DisplayName("Test for Shizene Assignment 14")
    public void testGreeterShizeneA14()

    {
        g.setName("Shizene");
        assertEquals(g.getName(),"Shizene");
        assertEquals(g.sayHello(),"Hello Shizene!");
    }
    
    @Test
    @DisplayName("Test for Name = 'SteveA14'")
    public void  testGreeter14() 
    {
        g.setName("SteveA14");
        assertEquals(g.getName(), "SteveA14");
       // assertEquals(g.sayHello(), "Hello SteveA14");
    }
}

